from flask import Flask
from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from pgvector.sqlalchemy import Vector
from sqlalchemy.orm import mapped_column

from .server.config import settings

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = settings.SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = settings.SQLALCHEMY_TRACK_MODIFICATIONS

db = SQLAlchemy(app)
migrate = Migrate(app, db)

Base = declarative_base()

class Todo(Base):
    __tablename__ = "todo"
    id = Column(Integer, primary_key=True)
    content = Column(String(length=2000))
    embedding = mapped_column(Vector())

    def __repr__(self):
        return '<Task %r>' % self.id
