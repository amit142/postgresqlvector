import os
from tempfile import TemporaryDirectory
from flask import Blueprint, render_template, request, redirect
import sqlalchemy

from server.database import Document, db
from server.embedding import get_embeddings_of_file, get_embeddings_of_query

router = Blueprint("routes", __name__, static_folder="../web/static", template_folder="../web/templates")

@router.route("/", methods=["POST", "GET"])
def index():
    if request.method == "POST":

        pdf_file = request.files["pdf_file"]

        if pdf_file:
            with TemporaryDirectory() as dir:
                with open(os.path.join(dir, "file.pdf"), mode="wb") as f:
                    pdf_file.save(f)
                texts, embeddings = get_embeddings_of_file(os.path.join(dir, "file.pdf"))
                for text, embedding in zip(texts, embeddings):
                    new_document = Document(content=text, embedding=embedding)
                    try:
                        db.session.add(new_document)
                        db.session.commit()
                        return redirect("/")
                    except Exception as e:
                        print("Error", e)
                        return "There was an issue adding your task"
        query = request.form.get("query")
        if query:
            query_embedding = get_embeddings_of_query(query)
            res = db.session.query(
                    Document,
                    Document.embedding.cosine_distance(query_embedding).label("distance"),  # type: ignore
                ).order_by(sqlalchemy.asc("distance")).all()
            return render_template("index.html", documents=res)
        return redirect("/")

    else:
        tasks = []
        return render_template("index.html", tasks=tasks)
