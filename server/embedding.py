from langchain.embeddings.openai import OpenAIEmbeddings
import os

from langchain.document_loaders import PyPDFLoader

embeddings = OpenAIEmbeddings()

def get_embeddings_of_query(query):
    return embeddings.embed_query(query)


def get_embeddings_of_file(path):
    loader = PyPDFLoader(path)
    texts = loader.load_and_split()
    texts_list = [t.page_content for t in texts]
    return texts_list, embeddings.embed_documents(texts_list)
