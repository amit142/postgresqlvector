from flask import Flask
from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, create_engine
from flask_sqlalchemy import SQLAlchemy
from pgvector.sqlalchemy import Vector
from sqlalchemy.orm import declarative_base, mapped_column, Session

from server.config import Settings


settings = Settings()

engine = create_engine(f"postgresql+psycopg2://{settings.POSTGRES_USER}:{settings.POSTGRES_PASSWORD}@{settings.POSTGRES_SERVER}:5432/{settings.POSTGRES_DB}")
app = Flask(__name__,  static_folder="../../web/static", template_folder="../../web/templates")
app.config["SQLALCHEMY_DATABASE_URI"] = settings.SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
    
db = SQLAlchemy(app)

Base = declarative_base()

class Document(Base):
    __tablename__ = "Documents"
    id = Column(Integer, primary_key=True)
    content = Column(String(length=2000))
    embedding = mapped_column(Vector())

    def __repr__(self):
        return "<Task %r>" % self.id

Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
